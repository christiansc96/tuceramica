<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta name="description" content="Tu Ceramica">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

    <!-- Page Title -->
    <title>{{ config('app.name') }} | @yield('title')</title>

    <!-- Favicon -->
    <link rel="icon" href="img/favicon.png" type="image/png" />

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <link rel="stylesheet" href="{{asset('css/media.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.transitions.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery.bxslider.css')}}">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/flexslider.css')}}">
    <link rel="stylesheet" href="{{asset('css/flexslider-set.css')}}">

    @yield('styles')


    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Loader Begin -->

    <div class="loader">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>

    <!-- Loader End -->

    <!-- Back To Top Begin -->

    <div id="back-top">
        <a href="#" class="scroll">
            <i class="arrow_carrot-up"></i>
        </a>
    </div>

    <!-- Back To Top End -->

    <!-- Site Wrapper Begin -->

    <div class="wrapper">

        <!-- Header Begin -->

        <header>
            <nav class="main-nav menu-dark menu-transparent nav-transparent">
                <div class="col-md-12">
                    <div class="navbar">
                        <div class="brand-logo">
                            <a href="{{ route('fe.home') }}" class="navbar-brand">
                                <img src="{{asset('img/logo.png')}}" alt="Tu Cerámica" />
                            </a>
                        </div>
                        <!-- brand-logo -->

                        <div class="navbar-header">
                            <div class="inner-nav right-nav">
                                <ul class="rightnav-links">
                                    <li>
                                        <a href="#" id="search-trigger"><i class="fa fa-search"></i></a>
                                        <form action="#" id="search" method="get" class="">
                                            <div class="input">
                                                <div class="container">
                                                    <input class="search" placeholder="Consiga el producto que busca..." type="text">
                                                    <button class="submit" type="submit" value="close"><i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                                <!-- /container -->
                                            </div>
                                            <!-- /input -->
                                            <button class="icon_close" id="close" type="reset"></button>
                                        </form>
                                        <!-- /form -->
                                    </li>

                                    <li>
                                        <a href="#" class="side-cart-toggle">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="notice-num">2</span>
                                        </a>
                                    </li>

                                    <li>
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse">
                                            <span class="sr-only"></span>
                                            <i class="fa fa-bars"></i>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <!-- /right-nav -->
                        </div>
                        <!-- navbar-header -->

                        <div class="custom-collapse navbar-collapse collapse inner-nav margin-left-100 pull-right">
                            <ul class="nav navbar-nav nav-links">
                                <li class="dropdown classic-dropdown"><a href="{{route('fe.home')}}" class="dropdown-toggle">Home</a></li>
                                <!-- /dropdown -->

                                <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Quiénes somos</a></li>
                                <!-- /dropdown -->

                                <li class="dropdown classic-dropdown"><a href="productos.html" class="dropdown-toggle" data-toggle="dropdown">Productos <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="submenu dropdown">
                                            <a href="productos.html" class="dropdown-toggle" data-toggle="dropdown">Categoria uno</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="productos.html">Sub-categoria uno</a>
                                                </li>
                                                <li><a href="productos.html">Sub-categoria dos</a>
                                                </li>
                                            </ul>
                                            <!-- /dropdown-menu -->
                                        </li>
                                        <!-- /submenu -->
                                        <li><a href="{{ route('fe.products') }}">Categoria dos</a>
                                        </li>
                                        <li><a href="productos.html">Categoria tres</a>
                                        </li>
                                    </ul>
                                    <!-- /dropdown-menu -->
                                </li>
                                <!-- /dropdown -->

                                <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog</a></li>
                                <!-- /dropdown -->

                                <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Contacto</a></li>
                                <!-- /dropdown -->
                                <!-- /dropdown -->
                            </ul>
                            <!-- /nav -->
                        </div>
                        <!-- /collapse -->
                    </div>
                    <!-- /navbar -->

                    <div class="side-cart">
                        <div class="close-btn text-center padding-vertical-30">
                            <a href="#" class="close-cart">Cerrar</a>
                        </div>
                        <!-- /close-btn -->

                        <div class="shoping-cart-box">
                            <div class="cart-product-list padding-bottom-20">
                                <div class="cart-product">
                                    <a href="#"><img src="img/productos/img1.png" alt="">
                                    </a>
                                    <div class="cart-product-info">
                                        <div class="product-name">
                                            <a href="#">Titulo del producto</a>
                                        </div>
                                        <!-- /product-name -->
                                        <div class="product-atributes padding-vertical-15">
                                            <p class="quantity">Cantidad: 02</p>
                                        </div>
                                        <!-- /product-atributes -->
                                        <div class="product-price">
                                            <p>160.000.000,00 Bs</p>
                                        </div>
                                        <!-- /product-price -->
                                        <a href="#" class="remove-btn"><i class="fa fa-close"></i></a>
                                    </div>
                                    <!-- /cart-product-info -->
                                </div>
                                <!-- /cart-product -->
                                <div class="cart-product">
                                    <a href="#"><img src="img/productos/img1.png" alt="">
                                    </a>
                                    <div class="cart-product-info">
                                        <div class="product-name">
                                            <a href="#">Titulo del producto</a>
                                        </div>
                                        <!-- /product-name -->
                                        <div class="product-atributes padding-vertical-15">
                                            <p class="quantity">Cantidad: 02</p>
                                        </div>
                                        <!-- /product-atributes -->
                                        <div class="product-price">
                                            <p>160.000.000,00 Bs</p>
                                        </div>
                                        <!-- /product-price -->
                                        <a href="#" class="remove-btn"><i class="fa fa-close"></i></a>
                                    </div>
                                    <!-- /cart-product-info -->
                                </div>
                                <!-- /cart-product -->
                            </div>
                            <!-- /cart-product-list -->

                            <div class="cart-prices padding-top-30">
                                <div class="shipping">
                                    <span class="shipping-text">Envio :</span>
                                    <span class="product-price pull-right">50.000.000,00 Bs</span>
                                </div>
                                <!-- /shipping -->
                                <div class="total-price padding-top-15">
                                    <span class="price-text">Total :</span>
                                    <span class="product-price pull-right">370.000.000,00 Bs</span>
                                </div>
                                <!-- /total-price -->
                            </div>
                            <!-- /cart-prices -->
                            <div class="cart-buttons margin-top-30">
                                <a href="carrito.html" class="view-cart-btn">Ver carrito</a>
                                <a href="carrito.html" class="checkout-btn margin-top-25">Comprar</a>
                            </div>
                            <!-- /cart-buttons -->
                        </div>
                        <!-- /shoping-cart-box -->
                    </div>
                    <!-- /side-cart -->
                </div>
                <!-- /container -->
            </nav>
            <!-- /nav -->
        </header>

        <!-- Header End -->

        @yield('content')

        <!-- Footer Begin -->

        <footer class="footer-2">
            <div class="container">
                <div class="row">
                    <div class="footer-social text-center">
                        <ul>
                            <li><a href="#">Facebook</a>
                            </li>
                            <li><a href="#">Twitter</a>
                            </li>
                            <li><a href="#">Instagram</a>
                            </li>
                            <li><a href="#" class="bd-right">YouTube</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /footer-social -->

                    <div class="col-md-6 col-sm-6 about-widget padding-top-50" style="margin-top: 2px;">
                        <img src="img/logo.png" alt="" />
                        <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, antepo suerit litterarum formas humanitatis per seacula quarta. Typi non habent claritatem insitam est usus legent is iis qui facit claritatem. Investigatione littera gothica, quam nunc putamus parum claram, antepo suerit litterarum formas humanitatis per seacula quarta.</p>
                        <ul>
                            <li><img src="{{asset('img/icon-pointer.png')}}" alt="" />aqui iria la direccion de la oficina principal</li>
                            <li><img src="{{asset('img/icon-phone.png')}}" alt="" />0212 123.4578</li>
                            <li><img src="{{asset('img/icon-mail.png')}}" alt="" />Contacto@tuceramica.com</li>
                        </ul>
                    </div>
                    <!-- /column -->

                    <div class="col-md-3 col-sm-6 twitter-feed padding-top-40">
                        <h3>twitter</h3>
                        <ul class="margin-top-60">
                            <li>Typi non habent claritatem insitam est usus legent is iis qui facit claritatem. Investigatione <a href="http://arquetitek.com" target="blank">http://arquetitek.com</a>
                                <span><i class="social_twitter"></i>12 days ago</span>
                            </li>
                            <li>Typi non habent claritatem insitam est usus legent is <a href="http://arquetitek.com" target="blank">http://arquetitek.com</a>
                                <span><i class="social_twitter"></i>10 days ago</span>
                            </li>
                        </ul>
                    </div>
                    <!-- /column -->

                    <div class="col-md-3 col-sm-6 news padding-top-40">
                        <h3>Blog</h3>
                        <ul class="margin-top-65">
                            <li>
                                <div class="news-image">
                                    <img src="{{asset('img/blog/blog1.jpg')}}" style="max-width: 75px;" alt="" />
                                </div>
                                <!-- /news-image -->
                                <div class="news-details">
                                    <span class="title"><a href="#">titulo de la publicacion</a></span>
                                    <span class="date">mayo 11, 2018</span>
                                </div>
                                <!-- /news-details -->
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="news-image">
                                    <img src="{{asset('img/blog/blog2.jpg')}}" style="max-width: 75px;" alt="" />
                                </div>
                                <!-- /news-image -->
                                <div class="news-details">
                                    <span class="title"><a href="#">titulo de la publicacion</a></span>
                                    <span class="date">mayo 11, 2018</span>
                                </div>
                                <!-- /news-details -->
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                        <!-- /ul -->
                    </div>
                    <!-- /column -->

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->

            <div class="footer-rights">
                <div class="container">
                    <div class="copyright-info text-center">
                        <p class="ft-desc">Copyright 2018 Tuceramica all rights reserved. Desarrollado por<a href="http://arquetitek.com" target="blank"> Arquetitek</a>
                        </p>
                    </div>
                    <!-- /copyright-info -->
                    <div class="footer-payments pull-right">
                        <ul>
                            <li><img src="{{asset('img/visa.png')}}" alt="" />
                            </li>
                            <li><img src="{{asset('img/mastercard.png')}}" alt="" />
                            </li>
                            <li><img src="{{asset('img/discover.png')}}" alt="" />
                            </li>
                            <li><img src="{{asset('img/americanexpress.png')}}" alt="" />
                            </li>
                            <li><img src="{{asset('img/paypal.png')}}" alt="" />
                            </li>
                        </ul>
                    </div>
                    <!-- /footer-payments -->
                </div>
                <!-- /container -->
            </div>
            <!-- /footer-rights -->
        </footer>

        <!-- Footer End -->


    </div>

    <!-- Site Wrapper End -->

    <!--- Scripts -->

    <script src="{{asset('js/lib/jquery.min.js')}}"></script>
    <script src="{{asset('js/vendors/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('js/lib/moderniz.min.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    <script src="{{asset('js/vendors/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.bxslider.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.flexslider-min.js')}}"></script>
    <script src="{{asset('js/vendors/flexslider-init.js')}}"></script>
    <script src="{{asset('js/vendors/smoothscroll.js')}}"></script>

    @yield('scripts')

    <script type="text/javascript">
        $(window).on('load', function() {
            $('#newsletterModal').modal('show');
        });
    </script>

</body>
</html>