<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>
            {{ config('app.name') }} | @yield('title')
        </title>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
            <meta content="width=device-width, initial-scale=1" name="viewport"/>
            <meta content="Tu cerámica dashboard" name="description"/>
            <meta content="arquetitek" name="author"/> 
            <!-- CSRF Token -->
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <!-- BEGIN GLOBAL MANDATORY STYLES -->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="{{asset('assets/global/css/components.min.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css"/>
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="{{asset('assets/layouts/layout3/css/layout.min.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/layouts/layout3/css/themes/default.min.css')}}" id="style_color" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/layouts/layout3/css/custom.min.css')}}" rel="stylesheet" type="text/css"/>
            {{-- SweetAlert2 --}}
            <script src="{{asset('assets/sweetalert2/sweetalert2.min.js')}}"></script>
            <link href="{{asset('assets/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet">
            @yield('styles')
            <!-- END THEME LAYOUT STYLES -->
            <link rel="icon" href="{{asset('img/favicon.png')}}" type="image/png" />
        </meta>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <!-- BEGIN HEADER TOP -->
                        <div class="page-header-top">
                            <div class="container">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                    <a href="{{ route('fe.home') }}">
                                        <img alt="Tu Cerámica" class="logo-default" src="{{ asset('img/logo-default.png') }}">
                                        </img>
                                    </a>
                                </div>
                                <!-- END LOGO -->
                                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                <a class="menu-toggler" href="javascript:;">
                                </a>
                                <!-- END RESPONSIVE MENU TOGGLER -->
                                <!-- BEGIN TOP NAVIGATION MENU -->
                                <div class="top-menu">
                                    <ul class="nav navbar-nav pull-right">
                                        <!-- BEGIN NOTIFICATION DROPDOWN -->
                                        <!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                                        <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                                        <!-- BEGIN USER LOGIN DROPDOWN -->
                                        <li class="dropdown dropdown-user dropdown-dark">
                                            <a class="dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="javascript:;">
                                                <img alt="{{ auth()->user()->image->name }}" class="img-circle" src="
                                                                                    @if(auth()->user()->image->id == 1)
                                                                                        {{asset(auth()->user()->image->name)}}
                                                                                    @else
                                                                                        {{asset('storage/images/users/'.auth()->user()->id.'/'.auth()->user()->image->name)}}
                                                                                    @endif
                                                                                ">
                                                    <span class="username username-hide-mobile">
                                                        {{ auth()->user()->username }}
                                                    </span>
                                                </img>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-default">
                                                <li>
                                                    <a href="{{ url('/') }}">
                                                        <i class="fa fa-home">
                                                        </i>
                                                        Homepage
                                                    </a>
                                                </li>
                                                <li class="divider">
                                                </li>
                                                <li>
                                                    <a href="{{ route('profile.user', auth()->user()->username) }}">
                                                        <i class="icon-user">
                                                        </i>
                                                        My Profile
                                                    </a>
                                                </li>
                                                <li class="divider">
                                                </li>
                                                <li>
                                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        <i class="icon-key">
                                                        </i>
                                                        Log Out
                                                    </a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- END USER LOGIN DROPDOWN -->
                                        <!-- END QUICK SIDEBAR TOGGLER -->
                                    </ul>
                                </div>
                                <!-- END TOP NAVIGATION MENU -->
                            </div>
                        </div>
                        <!-- END HEADER TOP -->
                        <!-- BEGIN HEADER MENU -->
                        <div class="page-header-menu">
                            <div class="container">
                                <!-- BEGIN HEADER SEARCH BOX -->
                                {{-- <form action="page_general_search.html" class="search-form" method="GET">
                                    <div class="input-group">
                                        <input class="form-control" name="query" placeholder="Search" type="text">
                                            <span class="input-group-btn">
                                                <a class="btn submit" href="javascript:;">
                                                    <i class="icon-magnifier">
                                                    </i>
                                                </a>
                                            </span>
                                        </input>
                                    </div>
                                </form> --}}
                                <!-- END HEADER SEARCH BOX -->
                                <!-- BEGIN MEGA MENU -->
                                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                                <div class="hor-menu ">
                                    <ul class="nav navbar-nav">
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if(Request::segment(2)=='dashboard') active @endif">
                                            <a href="{{ route('be.dashboard') }}">
                                                Dashboard
                                                <span class="arrow">
                                                </span>
                                            </a> 
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if(Request::segment(2)=='productos') active @endif">
                                            <a href="#">
                                                Productos
                                                <span class="arrow">
                                                </span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='productos') active @endif">
                                                    <a href="{{ route('admin-productos.index') }}" class="nav-link  "> Ver productos </a>
                                                </li>
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='colores') active @endif">
                                                    <a href="{{ route('admin-colores.index') }}" class="nav-link  "> Colores </a>
                                                </li>
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='medidas') active @endif">
                                                    <a href="{{ route('admin-medidas.index') }}" class="nav-link  "> Medidas </a>
                                                </li>
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='usos') active @endif">
                                                    <a href="{{ route('admin-usos.index') }}" class="nav-link  "> Usos </a>
                                                </li>
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='espacios') active @endif">
                                                    <a href="{{ route('admin-espacios.index') }}" class="nav-link  "> Espacios </a>
                                                </li>
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='anti-resbalantes') active @endif">
                                                    <a href="{{ route('admin-anti-resbalantes.index') }}" class="nav-link  "> Anti-resbalantes </a>
                                                </li>
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='marcas') active @endif">
                                                    <a href="{{ route('admin-marcas.index') }}" class="nav-link  "> Marcas </a>
                                                </li>
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='pastas') active @endif">
                                                    <a href="{{ route('admin-pastas.index') }}" class="nav-link  "> Pastas </a>
                                                </li>
                                                <li aria-haspopup="true" class="@if(Request::segment(2)=='texturas') active @endif">
                                                    <a href="{{ route('admin-texturas.index') }}" class="nav-link  "> Texturas </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if(Request::segment(2)=='admin-pedidos') active @endif">
                                            <a href="#">
                                                Pedidos
                                                <span class="arrow">
                                                </span>
                                            </a>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if(Request::segment(2)=='admin-usuarios') active @endif">
                                            <a href="{{ route('admin-usuarios.index') }}">
                                                Usuarios
                                                <span class="arrow">
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END MEGA MENU -->
                            </div>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!-- END HEADER -->
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    @yield('content')
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <!-- BEGIN FOOTER -->
                    <!-- BEGIN PRE-FOOTER -->
                    <!--<div class="page-prefooter">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                                    <h2>
                                        About
                                    </h2>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam dolore.
                                    </p>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs12 footer-block">
                                    <h2>
                                        Subscribe Email
                                    </h2>
                                    <div class="subscribe-form">
                                        <form action="javascript:;">
                                            <div class="input-group">
                                                <input class="form-control" placeholder="mail@email.com" type="text">
                                                    <span class="input-group-btn">
                                                        <button class="btn" type="submit">
                                                            Submit
                                                        </button>
                                                    </span>
                                                </input>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                                    <h2>
                                        Follow Us On
                                    </h2>
                                    <ul class="social-icons">
                                        <li>
                                            <a class="rss" data-original-title="rss" href="javascript:;">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="facebook" data-original-title="facebook" href="javascript:;">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="twitter" data-original-title="twitter" href="javascript:;">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="googleplus" data-original-title="googleplus" href="javascript:;">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="linkedin" data-original-title="linkedin" href="javascript:;">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="youtube" data-original-title="youtube" href="javascript:;">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="vimeo" data-original-title="vimeo" href="javascript:;">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                                    <h2>
                                        Contacts
                                    </h2>
                                    <address class="margin-bottom-40">
                                        Phone: 800 123 3456
                                        <br>
                                            Email:
                                            <a href="mailto:info@metronic.com">
                                                info@metronic.com
                                            </a>
                                        </br>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <!-- END PRE-FOOTER -->
                    <!-- BEGIN INNER FOOTER -->
                    <div class="page-footer">
                        <div class="container">
                            2018 © Desarrollado por
                            <a href="http://arquetitek.com" target="_blank">
                                Arquetitek
                            </a>
                            |   Christians Cartaya
                        </div>
                    </div>
                    <div class="scroll-to-top">
                        <i class="icon-arrow-up">
                        </i>
                    </div>
                    <!-- END INNER FOOTER -->
                    <!-- END FOOTER -->
                </div>
            </div>
        </div>
        <!-- BEGIN QUICK NAV -->
        <!-- <nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
            <ul>
                <li>
                    <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank" class="active">
                        <span>Purchase Metronic</span>
                        <i class="icon-basket"></i>
                    </a>
                </li>
                <li>
                    <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/reviews/4021469?ref=keenthemes" target="_blank">
                        <span>Customer Reviews</span>
                        <i class="icon-users"></i>
                    </a>
                </li>
                <li>
                    <a href="http://keenthemes.com/showcast/" target="_blank">
                        <span>Showcase</span>
                        <i class="icon-user"></i>
                    </a>
                </li>
                <li>
                    <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                        <span>Changelog</span>
                        <i class="icon-graph"></i>
                    </a>
                </li>
            </ul>
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav> -->
        <div class="quick-nav-overlay">
        </div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="https://unpkg.com/vue@latest"></script>
        <script src="https://unpkg.com/vee-validate@2.0.0-rc.7/dist/vee-validate.js" type="text/javascript">
        </script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script type="text/javascript">
            Vue.use(VeeValidate);
        </script>
        <script src="{{asset('assets/global/plugins/jquery.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/global/plugins/js.cookie.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript">
        </script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{asset('assets/global/scripts/app.min.js')}}" type="text/javascript">
        </script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{asset('assets/layouts/layout3/scripts/layout.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/layouts/layout3/scripts/demo.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript">
        </script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/pages/scripts/ui-bootbox.min.js')}}" type="text/javascript"></script>
        @yield('scripts')
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>
</html>