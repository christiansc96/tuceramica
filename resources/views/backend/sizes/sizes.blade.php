@extends('layouts.back')

@section('title', 'admin medidas')

@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
{{-- dataTables --}}
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Medidas
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">
                            Dashboard
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <a href="{{ route('admin-productos.index') }}">
                            Productos
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <span>
                            Medidas
                        </span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner" id="sizes" >
                    <div class="portlet box dark">
				        <div class="portlet-title">
				            <div class="caption">
				                Ultimas medidas agregados
				            </div>
				            <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
				                Agregar medida
				            </a>
				        </div>
				        <div class="portlet-body flip-scroll">
				            <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="size-table">
				                <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
				                    <tr>
                                        <th>id</th>
				                        <th>nombre</th>
				                        <th width="10%">estatus</th>
				                        <th width="10%">opciones</th>
				                    </tr>
				                </thead>
				            </table>
				        </div>
				    </div>
				    @include('backend.sizes.form-size')
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
{!!Form::open(['route'=>[ 'superadmin.datatable.status.size', ':SIZE_ID'],'method'=>'GET', 'id' => 'form-estatus'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-medidas.destroy', ':SIZE_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-medidas.edit', ':SIZE_ID' ],'method'=>'GET', 'id' => 'form-edit'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-medidas.update', ':SIZE_ID' ],'method'=>'PATCH', 'id' => 'form-update'])!!}
{!!Form::close()!!}
<!-- END CONTAINER -->
@endsection

@section('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>

<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script>
var medida = new Vue({
    el: '#sizes',
    data: {
        id: null,
        name: null,
        save_method: null,
        method: null,
    },
    methods: {
        addForm: function(){
            medida.save_method = 'add';
            medida.method = 'POST';
            $('#modal-form').modal('show');
            medida.id = null;
            medida.name = null;
            $('.modal-title').text('Agregar medida');
        },
        modal_form: function(){

            this.$validator.validateAll().then((result) => {
                if(result){

                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });

                    let formData = new FormData();

                    if (medida.save_method == 'add'){

                        var form = $('#form-post');

                        var url = form.attr('action');

                    }else{

                        var form = $('#form-update');

                        var url = form.attr('action').replace(':SIZE_ID', medida.id);

                        formData.append('id', medida.id);

                    } 

                    
                    formData.append('_method', medida.method);
                    formData.append('name', medida.name);

                    axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                    }).then(function (response) {
                        console.log(response);
                        dialog.modal('hide');
                        table.ajax.reload();
                        swal({
                            title: 'Successfully!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                        $('#modal-form').modal('hide');
                    }).catch(function (error) {
                        console.log(error);
                        dialog.modal('hide');
                        var refrescar = bootbox.dialog({
                            title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                            message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                            closeButton: false,
                            buttons: {
                                refrescar: {
                                    label: '<i class="fa fa-refresh"></i> Refrescar',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                    return;
                }
                bootbox.alert({
                    message: "<p class='text-center'>Debe corregir los errores</p>",
                    backdrop: true,
                    closeButton: false,
                });
            });
        }
    }
});

var table = $('#size-table').DataTable({
				"language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "_MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ de _TOTAL_ entradas",
                    "sInfoEmpty":      "Mostrando de 0 a 0 de 0 entradas",
                    "sInfoFiltered":   "(filtrado de _MAX_ entradas totales)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
				processing: true,
				serverSide: true,
				ajax: "{{ route('superadmin.datatable.sizes') }}",
				columns: [
                    {data: 'id', name: 'id'},
					{data: 'name', name: 'name'},
					{data: 'estatus', name: 'estatus'},
					{data: 'opciones', name: 'opciones', orderable: false, searchable: false}
				]
            });

function editForm(id)
{
    var form = $('#form-edit');
    var url = form.attr('action').replace(':SIZE_ID', id);
    var data = form.serialize();
    medida.save_method = 'edit';
    medida.method = 'PATCH';
    axios.get(url, data)
    .then(function (response) {
        $('#modal-form').modal('show');
        $('.modal-title').text('Editar medida');
        medida.id = response.data.id;
        medida.name = response.data.name;
    }).catch(function (error) {
        console.log(error);
            dialog.modal('hide');
            var refrescar = bootbox.dialog({
                title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                closeButton: false,
                buttons: {
                    refrescar: {
                        label: '<i class="fa fa-refresh"></i> Refrescar',
                        callback: function (result) {
                            location.reload(true);
                        }
                    }
                }
            });
    });
}

function statusData(id)
{
    var reactive = bootbox.dialog({
        message: "<p class='text-center'>Seguro?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    var form = $('#form-estatus');
                    var url = form.attr('action').replace(':SIZE_ID', id);
                    var data = form.serialize();
                    axios.get(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Estatus actualizado!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refrescar = bootbox.dialog({
			                title: "<p class='text-center'>Un error ha ocurrido :(</p>",
			                message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
			                closeButton: false,
			                buttons: {
			                    refrescar: {
			                        label: '<i class="fa fa-refresh"></i> Refrescar',
			                        callback: function (result) {
			                            location.reload(true);
			                        }
			                    }
			                }
			            });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}

function removeData(id)
{
    var form = $('#form-delete');
    var url = form.attr('action').replace(':SIZE_ID', id);
    var data = form.serialize();

    var eliminar = bootbox.dialog({
        title: "<p class='text-center'>Quiere remover este <strong>medida</strong>?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    axios.delete(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Removido!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refrescar = bootbox.dialog({
			                title: "<p class='text-center'>Un error ha ocurrido :(</p>",
			                message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
			                closeButton: false,
			                buttons: {
			                    refrescar: {
			                        label: '<i class="fa fa-refresh"></i> Refrescar',
			                        callback: function (result) {
			                            location.reload(true);
			                        }
			                    }
			                }
			            });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
</script>
@endsection

