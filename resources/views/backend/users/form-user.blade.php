<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {!!Form::open(['route'=>[ 'admin-usuarios.store'],'method'=>'POST', 'id' => 'form-post', 'class' => 'form-horizontal', 'files'=>true, 'data-vv-scope' => 'form-1'])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <div class="form-group" :class="{'has-error': errors.has('form-1.role') }" id="role">
                        <label for="role" class="col-md-3 control-label">Rol</label>
                        <div class="col-md-6">
                            <select class="form-control" name="role" v-model="role" v-validate="'required|max:255'" autofocus>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{ucfirst($role->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('form-1.role')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.role') }}
                            </span>
                        </div>
                    </div>
                    <div class="form-group" :class="{'has-error': errors.has('form-1.fullname') }" id="fullname">
                        <label for="fullname" class="col-md-3 control-label">Nombre completo</label>
                        <div class="col-md-6">
                            <input class="form-control" name="fullname" type="text" v-model="fullname" v-validate="'required|alpha_spaces|max:255'"/>
                            <span v-show="errors.has('form-1.fullname')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.fullname') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.username') }" id="username">
                        <label for="username" class="col-md-3 control-label">Username</label>
                        <div class="col-md-6">
                            <input class="form-control" name="username" type="text" v-model="username" v-validate="'required|alpha_num|alpha_dash|max:255'"/> 
                            <span v-show="errors.has('form-1.username')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.username') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.phone') }" id="phone">
                        <label for="phone" class="col-md-3 control-label">Teléfono</label>
                        <div class="col-md-6">
                            <input class="form-control" name="phone" type="text" v-model="phone" v-validate="{required: true, max: 255, regex: /\(?([0-9\-\+]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/}"/> 
                            <span v-show="errors.has('form-1.phone')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.phone') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.email') }" id="email">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-6">
                            <input class="form-control" name="email" type="email" v-model="email" v-validate="'required|email|max:255'"/> 
                            <span v-show="errors.has('form-1.email')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.email') }}
                            </span>
                        </div>
                    </div>

                    <hr id="divider"></hr>

                    <div class="form-group" id="password" :class="{'has-error': errors.has('form-1.password') }">
                        <label for="password" class="col-md-3 control-label">Contraseña</label>
                        <div class="col-md-6">
                            <input class="form-control" name="password" type="password" v-model="password" v-validate="'alpha_num|max:18|min:6'"/> 
                            <span v-show="errors.has('form-1.password')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.password') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" id="confirm_password" :class="{'has-error': errors.has('form-1.confirm_password') }">
                        <label for="confirm_password" class="col-md-3 control-label">Confirmar Contraseña</label>
                        <div class="col-md-6">
                            <input class="form-control" name="confirm_password" type="password" v-model="confirm_password" v-validate="'confirmed:password'"/> 
                            <span v-show="errors.has('form-1.confirm_password')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.confirm_password') }}
                            </span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" @click="modal_form()" class="btn btn-primary btn-save">Enviar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>

            {!!Form::close()!!}
        </div>
    </div>
</div>
