<div class="row">

	<div class="col-md-6">
		<p>Nombre: <strong>{{ $product->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Código: <strong>{{ $product->code }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Marca: ({{ $product->brand->type }})<strong>{{ $product->brand->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Fabricado en: <strong>{{ $product->made_in }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Tipo de pasta: <strong>{{ $product->pasta->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>PEI: <strong>{{ $product->pei }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Anti-resbalante: <strong>{{ $product->antislip->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Textura: <strong>{{ $product->texture->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Empaque: <strong>{{ $product->packing }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Precio: <strong>{{ $product->price->price }} {{ $product->price->coin }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Descripción: {{ $product->description}}</p>
	</div>

	<div class="col-md-6">
		<p>Descripción corta: {{ $product->short_description}}</p>
	</div>

	<div class="col-md-6">
		<p>Info: {{ $product->info }}</p>
	</div>

	<div class="col-md-6">
		<p>Colores: |@foreach($product->color as $color)<strong>{{ $color->name }}</strong>|@endforeach</p>
	</div>

	<div class="col-md-6">
		<p>Medidas: |@foreach($product->size as $size)<strong>{{ $size->name }}</strong>|@endforeach</p>
	</div>

	<div class="col-md-6">
		<p>Uso: |<strong>{{ $product->use->name }}</strong>|</p>
	</div>

	<div class="col-md-6">
		<p>Espacio: |<strong>{{ $product->space->name }}</strong>|</p>
	</div>

	<div class="col-md-6">
		<p>Fecha de creación: {{ $product->created_at }}</p>
	</div>
	
</div>