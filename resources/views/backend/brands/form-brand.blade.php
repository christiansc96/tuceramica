<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {!!Form::open(['route'=>[ 'admin-marcas.store'],'method'=>'POST', 'id' => 'form-post', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'files'=>true])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">

                    <div class="form-group" :class="{'has-error': errors.has('name') }">
                        <label for="name" class="col-md-3 control-label">Nombre</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('name')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('name')" data-container="body"></i>
                                <input type="text" id="name" name="name" v-model="name" class="form-control" v-validate="'required|max:191'">
                            </div>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('type') }">
                        <label for="type" class="col-md-3 control-label">Tipo</label>
                        <div class="col-md-6">
                            <select class="form-control" name="type" v-model="type" v-validate="'required|max:255'">
                                <option value="aleluia" selected>Aleluia</option>
                                <option value="rustimateriales">Rustimateriales</option>
                            </select>
                            <span v-show="errors.has('type')" style="color: red;" class="help is-danger">
                                @{{ errors.first('type') }}
                            </span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" @click="modal_form()" class="btn btn-primary btn-save">Enviar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>

            {!!Form::close()!!}
        </div>
    </div>
</div>
