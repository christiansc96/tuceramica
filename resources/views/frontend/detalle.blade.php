@extends('layouts.front')

@section('title')
    {{ $rproduct->name }}
@endsection

@section('content')

        <!-- Header End -->

        <!-- Page Header Begins -->

        <div id="page-header">
            <div class="header-bg-parallax" style="margin-top: -90px;">
                <div class="overlay">
                    <div class="container text-center">
                        <div class="header-description">
                            <h1>Dettales del Producto</h1>
                            <div class="breadcrumbs">
                                <ul>
                                    <li><a href="index.html">home</a>
                                    </li>
                                    <li><a href="index.html">Productos</a>
                                    </li>
                                    <li><a href="#" class="active">Detalles del producto</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /header-small-nav -->
                        </div>
                        <!-- /header-description -->
                    </div>
                    <!-- /container -->
                </div>
                <!-- /overlay -->
            </div>
            <!-- /product-filter -->
        </div>

        <!-- Page Header End -->

        <!-- Product Detail Begin -->

        <div class="p-details">
            <div class="container padding-top-100 padding-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product">
                            <div class="col-md-8">
                                <div class="product-image">
                                    <div class="product-quickview-slider">
                                        <ul class="slides">
                                            <li data-thumb="{{ asset('storage/images/products/'.$rproduct->id.'/'.$rproduct->image[0]->name.'') }}">
                                                @foreach($rproduct->image as $image)
                                                    <img src="{{ asset('storage/images/products/'.$rproduct->id.'/'.$image->name.'') }}" title="{{ $image->name }}" />
                                                @endforeach
                                            </li>
                                        </ul>
                                        <!-- /slides -->
                                    </div>
                                    <!-- /product-quickview-slider -->
                                </div>
                                <!-- /product-image -->
                            </div>
                            <!-- /column-->

                            <div class="col-md-4">
                                <div class="product-details">
                                    <div class="product-title">
                                        <p><a href="#">{{ $rproduct->name }}</a>
                                        </p>
                                    </div>
                                    <!-- /product-title -->

                                    <div class="product-small-detail padding-vertical-20">
                                        <p>{{ $rproduct->short_description }} </p>
                                    </div>
                                    <!-- /product-small-detail-->

                                    <div class="product-price padding-bottom-20">
                                        <p>{{ $rproduct->price->price }} {{ $rproduct->price->coin }}</p>
                                        {{-- <del>Bs 210.000,00</del> --}}
                                    </div>
                                    <!-- /product-price -->

                                    <div class="product-list-actions padding-top-20 padding-bottom-20">
                                        <form class="padding-bottom-25">
                                            <div>
                                                <div class="col-md-6" style="padding: 0;">
                                                    <div class="form-group">
                                                        <label for="p_size">Tamaño</label>
                                                        <select name="p_size" id="p_size" class="form-control">
                                                            @foreach($rproduct->size as $size)
                                                                <option value="{{ $size->id }}">{{ $size->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <!-- /form-control -->
                                                    </div>
                                                </div>
                                                <!-- /form-group -->

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="p_color">Color</label>
                                                        <select name="p_color" id="p_color" class="form-control">
                                                            @foreach($rproduct->color as $color)
                                                                <option value="{{ $color->id }}">{{ $color->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <!-- /form-control -->
                                                    </div>
                                                </div>
                                            </div>
                                                <!-- /form-group -->
                                        </form>
                                        <!-- /form -->

                                        <div class="product-quantity padding-top-30">
                                            <div class="quantity">
                                                <input type="button" value="-" class="minus">
                                                <input type="text" value="02" class="quantity-number">
                                                <input type="button" value="+" class="plus">
                                            </div>
                                            <!-- /quantity -->
                                        </div>
                                        <!-- /product-quantity -->

                                        <div class="product-cart margin-left-30 margin-top-30 padding-bottom-10">
                                            <a href="#">
                                                <p>+ Agregar al carrito</p>
                                            </a>
                                        </div>
                                        <!-- /product-cart -->

                                        <div class="social-share padding-vertical-30">
                                            <p>Compartir:</p>
                                            <div class="share">
                                                <a href="#"><i class="fa fa-facebook"></i>
                                               </a>
                                                <a href="#"><i class="fa fa-twitter"></i>
                                               </a>
                                            </div>
                                            <!-- /share -->
                                        </div>
                                        <!-- /social-share -->

                                        </div>
                                        <!-- /product-category-tag -->
                                    </div>
                                    <!-- /product-list-actions -->
                                </div>
                                <!-- /product-details -->
                            </div>
                            <!-- /column -->
                        </div>
                        <!-- /product -->
                    </div>
                    <!-- /column -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>

        <!-- Product Detail End -->

        <!-- Tabs Begin -->

        <div class="container padding-bottom-100">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs">
                        <ul class="tab-links text-center">
                            <li class="active">
                                <a href="#tab1">
                                    <span>Descripción larga</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#tab2">
                                    <span>Informacion Adicional</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#tab3">
                                    <span>Catálogo</span>
                                </a>
                            </li>
                        </ul>
                        <!-- /tab-links -->

                        <div class="tab-content">
                            <div id="tab1" class="tab active">
                                <div class="tab-description">
                                    <p class="padding-vertical-40">{{ $rproduct->description }}</p>

                                </div>
                                <!-- /tab-description -->
                            </div>
                            <!-- /tab -->

                            <div id="tab2" class="tab">
                                <div class="tab-description">
                                    <p class="padding-vertical-40">{{ $rproduct->info }}</p>

                                </div>
                                <!-- /tab-description -->
                            </div>
                            <!-- /tab -->

                            <div id="tab3" class="tab">
                                <div class="tab-description">
                                    <p class="padding-vertical-40">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when anunknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>

                                </div>
                                <!-- /tab-description -->
                            </div>
                            <!-- /tab -->
                        </div>
                        <!-- /tab-content -->
                    </div>
                    <!-- /tabs -->
                </div>
                <!-- /column -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->

        <!-- Tabs End -->

        <!-- Related Products Begin -->

        <div class="related-products">
            <div class="container text-center padding-top-20 padding-bottom-100">
                <h2 class="double-line"><span>Related Products</span></h2>
                <p class="sub-tittle">Eodem modo typi, qui nunc nobis videntur parum clari</p>
                <div class="row padding-top-60">
                    @if($results)
                        @foreach($results as $result) 
                            <div class="col-md-3 col-sm-6">
                                <div class="product-item padding-bottom-60">
                                    <div class="product-image">
                                        <img src="{{ asset('storage/images/products/'.$result['id'].'/'.$result['image'][0]['name'].'') }}" alt="{{ $result['image'][0]['name'] }}" title="{{ $result['image'][0]['name'] }}">
                                            <div class="product_overlay">
                                                <div class="product-cart" style="padding-bottom: 14px;">
                                                    <a href="#">
                                                        <p>+ Agregar al carrito</p>
                                                    </a>
                                                </div>
                                                <!-- /product-cart -->
                                            </div>
                                            <!-- /product_overlay -->
                                        </div>
                                        <!-- /product-image -->
                                        <div class="product-short-detail padding-top-20">
                                            <div class="product-title">
                                                <p><a href="{{ route('fe.show.products', $result['name']) }}">{{ $result['name'] }}</a>
                                                </p>
                                            </div>
                                            <!-- /product-title -->
                                            <div class="product-price">
                                                <p>{{ $result['price'] }} Bs</p>
                                            </div>
                                            <!-- /product-price -->
                                        </div>
                                        <!-- /product-short-detail -->
                                </div>
                                <!-- /product-item -->
                            </div>
                        @endforeach
                    @endif
                    <!-- /column -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>

        <!-- Related Products End -->
@endsection