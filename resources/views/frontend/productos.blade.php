@extends('layouts.front')

@section('title', 'Productos')

@section('styles')
@endsection

@section('content')
<!-- Header End -->
<!-- Page Header Begins -->
<div id="page-header">
    <div class="header-bg-parallax" style="margin-top: -90px;">
        <div class="overlay">
            <div class="container text-center">
                <div class="header-description">
                    <h1>
                        Nuestros productos
                    </h1>
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="index.html">
                                    home
                                </a>
                            </li>
                            <li>
                                <a class="active" href="#">
                                    Productos
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /header-small-nav -->
                </div>
                <!-- /header-description -->
            </div>
            <!-- /container -->
        </div>
        <!-- /overlay -->
    </div>
    <!-- /product-filter -->
</div>
<!-- Page Header End -->
<!-- Product Item List Begin -->
<div class="product-item-list">
    <div class="container-fluid padding-vertical-60">
        <div class="row" style="padding:0 25px">
            <!-- Siderbar -->
            <div class="col-md-2 col-sm-4 sidebar">
                <div class="sidebar-search padding-bottom-10">
                    <h4>
                        Buscador
                    </h4>
                    <form class="product-search padding-top-40 padding-bottom-5">
                        <button class="btn-search" title="Buscar" type="submit">
                            <i class="fa fa-search">
                            </i>
                        </button>
                        <!-- /button -->
                        <input class="form-control" placeholder="Buscar producto..." type="text">
                        </input>
                    </form>
                    <!-- /product-search -->
                </div>
                <!-- /sidebar-search -->
                <div class="product-categories padding-vertical-5">
                    <h4>
                        Espacios
                    </h4>
                    <div class="product-categories-list">
                        <ul class="padding-top-20">
                            <li>
                                <a href="#">
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'sala') Sala @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'sala' && $space->product) {{ $space->product->count() }} @endif
                                    @endforeach
                                    @if(!$spaces[0]->product && $spaces[0]->name == 'sala')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                            <li>
                                <a href="#">
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'cocina') Cocina @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'cocina' && $space->product) {{ $space->product->count() }} @endif
                                    @endforeach
                                    @if(!$spaces[1]->product && $spaces[1]->name == 'cocina')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                            <li>
                                <a href="#">
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'baños') Baños @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'baños' && $space->product) {{ $space->product->count() }} @endif
                                    @endforeach
                                    @if(!$spaces[2]->product && $spaces[2]->name == 'baños')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                            <li>
                                <a href="#">
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'comedor') Comedor @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'comedor' && $space->product) {{ $space->product->count() }} @endif
                                    @endforeach
                                    @if(!$spaces[3]->product && $spaces[3]->name == 'comedor')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                            <li>
                                <a href="#">
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'habitación') Habitación @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'habitación' && $space->product) {{ $space->product->count() }} @endif
                                    @endforeach
                                    @if(!$spaces[4]->product && $spaces[4]->name == 'habitación')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                            <li>
                                <a href="#">
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'oficinas') Oficinas @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'oficinas' && $space->product) {{ $space->product->count() }} @endif
                                    @endforeach
                                    @if(!$spaces[5]->product && $spaces[5]->name == 'oficinas')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                            <li>
                                <a href="#">
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'exteriores') Exteriores @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($spaces as $space) 
                                        @if($space->name == 'exteriores' && $space->product) {{ $space->product->count() }} @endif
                                    @endforeach
                                    @if(!$spaces[6]->product && $spaces[6]->name == 'exteriores')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!-- /product-categories-list-->
                </div>
                <!-- /product-categories-->
                <div class="product-categories padding-vertical-5">
                    <h4>
                        Usos
                    </h4>
                    <div class="product-categories-list">
                        <ul class="padding-top-20">
                            <li>
                                <a href="#">
                                    @foreach($uses as $use) 
                                        @if($use->name == 'paredes') Paredes @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($uses as $use) 
                                        @if($use->name == 'paredes' && $use->product) {{ $use->product->count() }} @endif
                                    @endforeach
                                    @if(!$uses[0]->product && $uses[0]->name == 'paredes')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                            <li>
                                <a href="#">
                                    @foreach($uses as $use) 
                                        @if($use->name == 'pisos') Pisos @endif
                                    @endforeach
                                </a>
                                <span>
                                    @foreach($uses as $use) 
                                        @if($use->name == 'pisos' && $use->product) {{ $use->product->count() }} @endif
                                    @endforeach
                                    @if(!$uses[1]->product && $uses[1]->name == 'pisos')
                                        0
                                    @endif
                                    {{-- 10 --}}
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!-- /product-categories-list-->
                </div>
                <!-- /product-categories-->
                <div class="filter-price padding-vertical-5">
                    <h4>
                        Precio
                    </h4>
                    <div class="slider-range padding-top-50 padding-bottom-20">
                        <fieldset class="price-range" data-max="50000" data-min="0" data-step="25">
                            <div class="price-slider">
                            </div>
                            <p>
                                <input type="text" value="0"/>
                                <input type="text" value="50000"/>
                            </p>
                            <div class="filter-button">
                                <a href="#">
                                    Filtrar
                                </a>
                            </div>
                            <!-- /filter-button-->
                        </fieldset>
                        <!-- /product-categories-->
                    </div>
                    <!-- /slider-range -->
                </div>
                <!-- /filter-price -->
                <div class="color-options padding-vertical-5">
                    <h4>
                        Color
                    </h4>
                    <div class="color-list">
                        <ul class="padding-top-20">
                            @foreach($colors as $color)
                                <li>
                                    <a href="#">
                                        {{ ucfirst($color->name) }}
                                    </a>
                                    <span>
                                        @if($color->product){{ $color->product->count() }} @else 0 @endif
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /color-list-->
                </div>
                <!-- /color-options-->
                <div class="color-options padding-vertical-5">
                    <h4>
                        Tamaño
                    </h4>
                    <div class="color-list">
                        <ul class="padding-top-20">
                            @foreach($sizes as $size)
                                <li>
                                    <a href="#">
                                        {{ $size->name }}
                                    </a>
                                    <span>
                                        @if($size->product) {{ $size->product->count() }} @else 0 @endif
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /color-list-->
                </div>
                <!-- /color-options-->
            </div>
            <!-- /sidebar -->
            <!-- Product Item List Begin -->
            @if($products != 'hola')
                @foreach($products as $product)
                    <div class="product-item-list col-md-10 col-sm-8 padding-top-20 text-center">
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item padding-bottom-60">
                                <div class="product-image">
                                    <img alt="" src="storage/images/products/{{ $product->id }}/{{ $product->image[0]->name }}">
                                        <div class="product_overlay">
                                            <div class="product-cart" style="padding-bottom: 14px;">
                                                <a href="#">
                                                    <p>
                                                        + Agregar al carrito
                                                    </p>
                                                </a>
                                            </div>
                                            <!-- /product-cart -->
                                        </div>
                                        <!-- /product_overlay -->
                                    </img>
                                </div>
                                <!-- /product-image -->
                                <div class="product-short-detail padding-top-20">
                                    <div class="product-title">
                                        <p>
                                            <a href="{{ route('fe.show.products', $product->name) }}">
                                               {{ $product->name }}
                                            </a>
                                        </p>
                                    </div>
                                    <!-- /product-title -->
                                </div>
                                <!-- /product-short-detail -->
                            </div>
                            <!-- /product-item -->
                        </div>
                    </div>
                @endforeach
            @else
                <div class="product-item-list col-md-10 col-sm-8 padding-top-20 text-center">
                        <div class="col-md-3 col-sm-6">
                            No hay registros disponibles
                        </div>
                    </div>
            @endif
            <!-- /product-item-list -->
        </div>
    </div>
    <!-- /container -->
</div>
<!-- Product Item List End -->
@endsection

@section('scripts')

@endsection