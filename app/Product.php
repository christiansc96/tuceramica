<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	public function image()
	{
		return $this->belongsToMany('App\Image', 'image_product')->withTimestamps();
	}

	public function size()
	{
		return $this->belongsToMany('App\Size', 'size_product')->withTimestamps();
	}

	public function color()
	{
		return $this->belongsToMany('App\Color', 'color_product')->withTimestamps();
	}

	public function use()
	{
		return $this->belongsTo('App\Uso', 'use_id');
	}

	public function space()
	{
		return $this->belongsTo('App\Space', 'space_id');
	}

	public function price()
	{
		return $this->belongsTo('App\Price', 'price_id');
	}

	public function antislip()
	{
		return $this->belongsTo('App\AntiSlip', 'antislip_id');
	}

	public function brand()
	{
		return $this->belongsTo('App\Brand', 'brand_id');
	}

	public function pasta()
	{
		return $this->belongsTo('App\Pasta', 'pasta_id');
	}

	public function texture()
	{
		return $this->belongsTo('App\Texture', 'texture_id');
	}

    protected $table = 'products';
    protected $guarded = [];
}
