<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

	public function product()
    {
    	return $this->hasOne('App\Product', 'brand_id');
    }

    protected $table ='brands';

    protected $guarded = [];
}
