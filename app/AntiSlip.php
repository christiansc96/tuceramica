<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntiSlip extends Model
{
	public function product()
    {
    	return $this->hasOne('App\Product', 'antislip_id');
    }

    protected $table ='anti-slips';

    protected $guarded = [];
}
