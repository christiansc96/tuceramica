<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	public function product()
	{
		return $this->belongsToMany('App\Product', 'image_product')->withTimestamps();
	}

	public function user()
	{
		return $this->hasOne('App\User');
	}

    protected $table ='images';

    protected $guarded = [];
}
