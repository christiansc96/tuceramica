<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Texture extends Model
{
	public function product()
    {
    	return $this->hasOne('App\Product', 'texture_id');
    }

    protected $table ='textures';

    protected $guarded = [];
}
