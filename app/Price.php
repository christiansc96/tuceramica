<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public function product()
    {
    	return $this->hasOne('App\Product', 'price_id');
    }

    protected $table = 'prices';

    protected $guarded = [];
}