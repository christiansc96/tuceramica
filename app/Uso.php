<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uso extends Model
{
    public function product()
    {
    	return $this->hasOne('App\Product', 'use_id');
    }

    protected $table = 'uses';
    
    protected $guarded = [];
}
