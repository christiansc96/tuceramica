<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Image;
use App\Space;
use App\Uso;
use App\Size;
use App\Color;

class FrontController extends Controller
{
	/**
	 * [index description]
	 * @return [type] [description]
	 */
    public function index()
    {
    	return view('frontend.index');
    }

    public function products()
    {
    	
        if(Product::all()):
            $products = Product::all();
        else:
            $products = 'hola';
        endif;

    	$spaces = Space::all();

    	$uses = Uso::all();

        $colors = Color::all();

        $sizes = Size::all();

    	return view('frontend.productos', compact('products', 'spaces', 'uses', 'colors', 'sizes'));
    }

    public function show_product($product)
    {
        $xproducts = Product::all();

        $rproduct = Product::where('name', $product)->first();

        foreach($xproducts as $xproduct):

            //Share Related Products
            if($rproduct->id != $xproduct->id):
                if($rproduct->space->id == $xproduct->space->id || $rproduct->color->id == $xproduct->color->id || $rproduct->size->id == $xproduct->size->id || $rproduct->use->id == $xproduct->use->id):

                    //Related Products
                    $results[] = $xproduct; 
                endif;
            else:
                $results = null;
            endif;
        endforeach;


        return view('frontend.detalle', compact('rproduct', 'results'));
    }
}
