<?php
// Auth::routes();

Route::get('/', 'FrontController@index')->name('fe.home');

Route::get('productos', 'FrontController@products')->name('fe.products');

Route::get('productos/{product}', 'FrontController@show_product')->name('fe.show.products');