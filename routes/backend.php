<?php
Route::middleware(['auth'])->group(function () {
	Route::get('/dashboard', 'BackController@index')->name('be.dashboard');
	Route::middleware(['role:super administrador'])->group(function () {
		/*
		|------------------------------------------------------------------------------------------------
		| admin product routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('productos', 'ProductController')->names([
			'index' => 'admin-productos.index',
			'show' => 'admin-productos.show',
			'create' => 'admin-productos.create',
			'store' => 'admin-productos.store',
			'edit' => 'admin-productos.edit',
			'update' => 'admin-productos.update',
			'destroy' => 'admin-productos.destroy',
			'store_image' => 'admin-productos.store_image',
			'destroy_image' => 'admin-productos.destroy_image',
		]);
		Route::get('datatable/productos', 'ProductController@datatable')->name('superadmin.datatable.products');
		Route::get('estatus/productos/{id}', 'ProductController@status')->name('superadmin.datatable.status.product');

		/*
		|------------------------------------------------------------------------------------------------
		| admin color routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('colores', 'ColorController')->names([
			'index' => 'admin-colores.index',
			'show' => 'admin-colores.show',
			'create' => 'admin-colores.create',
			'store' => 'admin-colores.store',
			'edit' => 'admin-colores.edit',
			'update' => 'admin-colores.update',
			'destroy' => 'admin-colores.destroy',
		]);
		Route::get('datatable/colores', 'ColorController@datatable')->name('superadmin.datatable.colors');
		Route::get('estatus/colores/{id}', 'ColorController@status')->name('superadmin.datatable.status.color');

		/*
		|------------------------------------------------------------------------------------------------
		| admin size routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('medidas', 'SizeController')->names([
			'index' => 'admin-medidas.index',
			'show' => 'admin-medidas.show',
			'create' => 'admin-medidas.create',
			'store' => 'admin-medidas.store',
			'edit' => 'admin-medidas.edit',
			'update' => 'admin-medidas.update',
			'destroy' => 'admin-medidas.destroy',
		]);
		Route::get('datatable/medidas', 'SizeController@datatable')->name('superadmin.datatable.sizes');
		Route::get('estatus/medidas/{id}','SizeController@status')->name('superadmin.datatable.status.size');

		/*
		|------------------------------------------------------------------------------------------------
		| admin use routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('usos', 'UseController')->names([
			'index' => 'admin-usos.index',
			'show' => 'admin-usos.show',
			'create' => 'admin-usos.create',
			'store' => 'admin-usos.store',
			'edit' => 'admin-usos.edit',
			'update' => 'admin-usos.update',
			'destroy' => 'admin-usos.destroy',
		]);
		Route::get('datatable/usos', 'UseController@datatable')->name('superadmin.datatable.uses');
		Route::get('estatus/usos/{id}', 'UseController@status')->name('superadmin.datatable.status.use');

		/*
		|------------------------------------------------------------------------------------------------
		| admin space routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('espacios', 'SpaceController')->names([
			'index' => 'admin-espacios.index',
			'show' => 'admin-espacios.show',
			'create' => 'admin-espacios.create',
			'store' => 'admin-espacios.store',
			'edit' => 'admin-espacios.edit',
			'update' => 'admin-espacios.update',
			'destroy' => 'admin-espacios.destroy',
		]);
		Route::get('datatable/espacios', 'SpaceController@datatable')->name('superadmin.datatable.spaces');
		Route::get('estatus/espacios/{id}', 'SpaceController@status')->name('superadmin.datatable.status.space');

		/*
		|------------------------------------------------------------------------------------------------
		| admin anti-slip routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('anti-resbalantes', 'AntiSlipController')->names([
			'index' => 'admin-anti-resbalantes.index',
			'show' => 'admin-anti-resbalantes.show',
			'create' => 'admin-anti-resbalantes.create',
			'store' => 'admin-anti-resbalantes.store',
			'edit' => 'admin-anti-resbalantes.edit',
			'update' => 'admin-anti-resbalantes.update',
			'destroy' => 'admin-anti-resbalantes.destroy',
		]);
		Route::get('datatable/anti-resbalantes', 'AntiSlipController@datatable')->name('superadmin.datatable.anti-slips');
		Route::get('estatus/anti-resbalantes/{id}', 'AntiSlipController@status')->name('superadmin.datatable.status.anti-slip');

		/*
		|------------------------------------------------------------------------------------------------
		| admin brand routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('marcas', 'BrandController')->names([
			'index' => 'admin-marcas.index',
			'show' => 'admin-marcas.show',
			'create' => 'admin-marcas.create',
			'store' => 'admin-marcas.store',
			'edit' => 'admin-marcas.edit',
			'update' => 'admin-marcas.update',
			'destroy' => 'admin-marcas.destroy',
		]);
		Route::get('datatable/marcas', 'BrandController@datatable')->name('superadmin.datatable.brands');
		Route::get('estatus/marcas/{id}', 'BrandController@status')->name('superadmin.datatable.status.brand');


		/*
		|------------------------------------------------------------------------------------------------
		| admin pasta routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('pastas', 'PastaController')->names([
			'index' => 'admin-pastas.index',
			'show' => 'admin-pastas.show',
			'create' => 'admin-pastas.create',
			'store' => 'admin-pastas.store',
			'edit' => 'admin-pastas.edit',
			'update' => 'admin-pastas.update',
			'destroy' => 'admin-pastas.destroy',
		]);
		Route::get('datatable/pastas', 'PastaController@datatable')->name('superadmin.datatable.pastas');
		Route::get('estatus/pastas/{id}', 'PastaController@status')->name('superadmin.datatable.status.pasta');

		/*
		|------------------------------------------------------------------------------------------------
		| admin pasta routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('texturas', 'TextureController')->names([
			'index' => 'admin-texturas.index',
			'show' => 'admin-texturas.show',
			'create' => 'admin-texturas.create',
			'store' => 'admin-texturas.store',
			'edit' => 'admin-texturas.edit',
			'update' => 'admin-texturas.update',
			'destroy' => 'admin-texturas.destroy',
		]);
		Route::get('datatable/texturas', 'TextureController@datatable')->name('superadmin.datatable.textures');
		Route::get('estatus/texturas/{id}', 'TextureController@status')->name('superadmin.datatable.status.texture');

		/*
		|------------------------------------------------------------------------------------------------
		| admin galery routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('galeria', 'GaleryController')->names([
			'index' => 'admin-galeria.index',
			'show' => 'admin-galeria.show',
			'create' => 'admin-galeria.create',
			'store' => 'admin-galeria.store',
			'edit' => 'admin-galeria.edit',
			'update' => 'admin-galeria.update',
			'destroy' => 'admin-galeria.destroy',
		]);
		Route::get('datatable/espacios/{id}', 'GaleryController@datatable')->name('superadmin.datatable.galery.product');

		/*
		|------------------------------------------------------------------------------------------------
		| admin user routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('usuarios', 'UserController')->names([
			'index' => 'admin-usuarios.index',
			'show' => 'admin-usuarios.show',
			'create' => 'admin-usuarios.create',
			'store' => 'admin-usuarios.store',
			'edit' => 'admin-usuarios.edit',
			'update' => 'admin-usuarios.update',
			'destroy' => 'admin-usuarios.destroy',
		]);
		Route::get('datatable/usuarios', 'UserController@datatable')->name('superadmin.datatable.users');
		Route::get('estatus/usuarios/{id}', 'UserController@status')->name('superadmin.datatable.status.user');
	});

	/*
	|------------------------------------------------------------------------------------------------
	| admin profile-user routes
	|------------------------------------------------------------------------------------------------
	|
	*/
	Route::get('perfil/{username}', 'ProfileUserController@show')->name('profile.user');
	Route::get('perfil/ajustes/{username}', 'ProfileUserController@settings')->name('settings.user');
	Route::post('cuenta/actualizar-datos', 'ProfileUserController@update_info')->name('profile.user.updateInfo');
	Route::post('cuenta/actualizar-avatar', 'ProfileUserController@update_avatar')->name('profile.user.updateAvatar');
	Route::post('cuenta/actualizar-contraseña', 'ProfileUserController@update_pwd')->name('profile.user.updatePwd');
});