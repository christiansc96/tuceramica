<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
	    $this->truncateTables([
            'users',
            'textures',
            'pastas',
            'brands',
            'anti-slips',
            'roles',
	    	'spaces',
	    	'uses',
            'colors',
            'sizes',
            'images'
	   	]);
        
        $this->call(ImageSeed::class);
        $this->call(RoleSeed::class);
        $this->call(UserSeed::class);
        $this->call(SpaceSeed::class);
        $this->call(TextureSeed::class);
        $this->call(PastaSeed::class);
        $this->call(BrandSeed::class);
        $this->call(AntiSlipSeed::class);
        $this->call(UseSeed::class);
        $this->call(ColorSeed::class);
        $this->call(SizeSeed::class);
    }

    protected function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
