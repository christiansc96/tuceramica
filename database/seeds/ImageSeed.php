<?php

use Illuminate\Database\Seeder;
use App\Image;

class ImageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $image = new Image();

        $image->name = 'avatar.png';

        $image->active = 1;

        $image->save();
    }
}
