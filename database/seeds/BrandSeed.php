<?php

use Illuminate\Database\Seeder;
use App\Brand;

class BrandSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brand = new Brand();
        $brand->name = 'mapisa';
        $brand->type = 'aleluia';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'peronda';
        $brand->type = 'aleluia';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'itt ceramic';
        $brand->type = 'aleluia';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'ceramic';
        $brand->type = 'aleluia';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'keratec';
        $brand->type = 'aleluia';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'recer';
        $brand->type = 'aleluia';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'spazioideale';
        $brand->type = 'rustimateriales';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'ceramica italia';
        $brand->type = 'rustimateriales';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'sigman';
        $brand->type = 'rustimateriales';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'ladrillera cucuta';
        $brand->type = 'rustimateriales';
        $brand->active = 1;
        $brand->save();

        $brand = new Brand();
        $brand->name = 'lovados';
        $brand->type = 'rustimateriales';
        $brand->active = 1;
        $brand->save();
    }
}
