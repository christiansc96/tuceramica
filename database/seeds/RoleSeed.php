<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role              = new Role();
        $role->name        = 'super administrador';
        $role->description = 'dios del sistema';
        $role->active      = 1;
        $role->save();

        $role              = new Role();
        $role->name        = 'administrador';
        $role->description = 'semi-dios del sistema';
        $role->active      = 1;
        $role->save();

        $role              = new Role();
        $role->name        = 'vendedor';
        $role->description = 'vendedor del sistema';
        $role->active      = 1;
        $role->save();

        $role             = new Role();
        $role->name        = 'registrado';
        $role->description = 'usuario del sistema';
        $role->active      = 1;
        $role->save();
    }
}
