<?php

use Illuminate\Database\Seeder;
use App\AntiSlip;

class AntiSlipSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $antislip = new AntiSlip();
        $antislip->name = 'R-1';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-2';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-3';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-4';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-5';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-6';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-7';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-8';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-9';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-10';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-11';
        $antislip->active = 1;
        $antislip->save();

        $antislip = new AntiSlip();
        $antislip->name = 'R-12';
        $antislip->active = 1;
        $antislip->save();
    }
}
