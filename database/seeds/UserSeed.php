<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_vendedor = Role::where('name', 'vendedor')->first();
        $role_administrador  = Role::where('name', 'administrador')->first();
        $role_registrado  = Role::where('name', 'registrado')->first();
        $role_super_administrador  = Role::where('name', 'super administrador')->first();


        $user = new User();
        $user->fullname = 'vendedor';
        $user->username = 'vendedor';
        $user->phone = '+123455555';
        $user->email = 'vendedor@lle.com';
        $user->password = bcrypt('vendedor');
        $user->image_id  = 1;
        $user->active  = 1;
        $user->save();
        $user->roles()->attach($role_vendedor);

        $admin = new User();
        $admin->fullname = 'administrador';
        $admin->username = 'administrador';
        $admin->phone = '+1234555551';
        $admin->email = 'administrador@lle.com';
        $admin->password = bcrypt('administrador');
        $admin->image_id  = 1;
        $admin->active = 1;
        $admin->save();
        $admin->roles()->attach($role_administrador);

        $super_admin = new User();
        $super_admin->fullname = 'dario diaz';
        $super_admin->username = 'dario';
        $super_admin->phone = '+12345555512';
        $super_admin->email = 'dario@tuceramica.com';
        $super_admin->password = bcrypt('admin');
        $super_admin->image_id  = 1;
        $super_admin->active = 1;
        $super_admin->save();
        $super_admin->roles()->attach($role_super_administrador);

        $super_admin = new User();
        $super_admin->fullname = 'cartaya';
        $super_admin->username = 'chexpress';
        $super_admin->phone = '+1234552512';
        $super_admin->email = 'christians@tuceramica.com';
        $super_admin->password = bcrypt('admin');
        $super_admin->image_id  = 1;
        $super_admin->active = 1;
        $super_admin->save();
        $super_admin->roles()->attach($role_super_administrador);
    }
}
